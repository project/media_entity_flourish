A module that provides Flourish (https://flourish.studio/) integration through the provision of a new media source, field widget and formatter.
