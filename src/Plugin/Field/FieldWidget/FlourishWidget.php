<?php

declare(strict_types = 1);

namespace Drupal\media_entity_flourish\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class Flourish_Widget.
 *
 * @package Drupal\media_entity_flourish\Plugin\Field\FieldWidget
 *
 * @FieldWidget(
 *   id = "flourish",
 *   label = @Translation("Flourish"),
 *   field_types = {
 *     "flourish",
 *   },
 * )
 */
class FlourishWidget extends WidgetBase {

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $field_storage_definition = $items->getFieldDefinition()->getFieldStorageDefinition();
    $element['value'] = [
      '#type' => 'textfield',
      '#title' => $field_storage_definition->getPropertyDefinition('value')->getLabel(),
      '#required' => $field_storage_definition->getPropertyDefinition('value')->isRequired(),
      '#default_value' => $items[$delta]->value,
    ];
    $element['type'] = [
      '#type' => 'select',
      '#title' => $field_storage_definition->getPropertyDefinition('type')->getLabel(),
      '#options' => [
        'visualisation' => $this->t('Visualisation'),
        'story' => $this->t('Story'),
      ],
      '#required' => $field_storage_definition->getPropertyDefinition('type')->isRequired(),
      '#default_value' => $items[$delta]->type,
    ];
    $element['description'] = [
      '#type' => 'textfield',
      '#title' => $field_storage_definition->getPropertyDefinition('description')->getLabel(),
      '#description' => $field_storage_definition->getPropertyDefinition('description')->getDescription(),
      '#required' => $field_storage_definition->getPropertyDefinition('description')->isRequired(),
      '#default_value' => $items[$delta]->description,
    ];
    return $element;
  }

}
