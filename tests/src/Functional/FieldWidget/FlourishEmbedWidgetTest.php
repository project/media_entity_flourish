<?php

declare(strict_types = 1);

namespace Drupal\Tests\media_entity_flourish\Functional\FieldWidget;

use Drupal\Tests\media\Functional\MediaFunctionalTestBase;

/**
 * Class Flourish_Embed_Widget_Test.
 *
 * @package Drupal\Tests\media_entity_flourish\Functional\FieldWidget
 *
 * @group media_entity_flourish
 */
class FlourishEmbedWidgetTest extends MediaFunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field_ui',
    'media_entity_flourish',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    \Drupal::configFactory()
      ->getEditable('media.settings')
      ->set('standalone_url', TRUE)
      ->save(TRUE);

    $this->container->get('router.builder')->rebuild();
  }

  /**
   * A test for the render of the 'flourish' widget.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testRender(): void {
    $account = $this->drupalCreateUser(['create media']);
    $this->drupalLogin($account);
    $media_type = $this->createMediaType('flourish');

    $this->drupalGet('media/add/' . $media_type->id());
    $assert = $this->assertSession();
    $assert->pageTextContains('Flourish Id');
    $assert->pageTextContains('Flourish type');
    $assert->pageTextContains('Flourish description');
  }

}
