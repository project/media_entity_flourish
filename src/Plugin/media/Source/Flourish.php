<?php

declare(strict_types = 1);

namespace Drupal\media_entity_flourish\Plugin\media\Source;

use Drupal\field\FieldConfigInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;

/**
 * Class flourish.
 *
 * @package Drupal\media_entity_flourish\Plugin\media\Source
 *
 * @MediaSource(
 *   id = "flourish",
 *   label = @Translation("Flourish"),
 *   allowed_field_types = {"flourish"},
 *   description = @Translation("Provides Flourish."),
 *   default_thumbnail_filename = "flourish.png"
 * )
 */
class Flourish extends MediaSourceBase {

  /**
   * {@inheritDoc}
   */
  public function getMetadataAttributes(): array {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function createSourceField(MediaTypeInterface $type): FieldConfigInterface {
    return parent::createSourceField($type)->set('label', 'Flourish');
  }

  /**
   * A method to get the placeholder url.
   *
   * In instances where Flourish cannot be rendered, a placeholder is
   * put in place.
   *
   * @return string
   *   The placeholder url.
   */
  public function getPlaceholderUrl(): string {
    return \Drupal::config('media.settings')->get('icon_base_uri') . DIRECTORY_SEPARATOR . $this->pluginDefinition['default_thumbnail_filename'];
  }

}
