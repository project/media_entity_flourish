<?php

declare(strict_types = 1);

namespace Drupal\media_entity_flourish\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Class Flourish_Item.
 *
 * @package Drupal\media_entity_flourish\Plugin\Field\FieldFormatter
 *
 * @FieldType(
 *   id = "flourish",
 *   label = @Translation("Flourish"),
 *   default_widget = "flourish",
 *   default_formatter = "flourish_embed",
 * )
 */
class FlourishItem extends FieldItemBase {

  /**
   * {@inheritDoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = [];
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Flourish Id'))
      ->setRequired(TRUE);
    $properties['type'] = DataDefinition::create('string')
      ->setLabel(t('Flourish type'))
      ->setRequired(TRUE);
    $properties['description'] = DataDefinition::create('string')
      ->setLabel(t('Flourish description'))
      ->setDescription(t('The description will be used as the aria label. If a description is not supplied it defaults back to the media title.'))
      ->setRequired(FALSE);
    return $properties;
  }

  /**
   * {@inheritDoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'value' => [
          'description' => 'Flourish Id.',
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => 255,
        ],
        'type' => [
          'description' => 'Flourish type.',
          'type' => 'varchar',
          'not null' => TRUE,
          'length' => 255,
        ],
        'description' => [
          'description' => 'Flourish description.',
          'type' => 'varchar',
          'not null' => FALSE,
          'length' => 512,
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }

}
