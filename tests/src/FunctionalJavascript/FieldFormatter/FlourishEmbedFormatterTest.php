<?php

declare(strict_types = 1);

namespace Drupal\Tests\media_entity_flourish\FunctionalJavascript\FieldFormatter;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\media\Entity\Media;
use Drupal\Tests\media\FunctionalJavascript\MediaJavascriptTestBase;

/**
 * Class Flourish_Embed_Formatter_Test.
 *
 * @package Drupal\Tests\media_entity_flourish\FunctionalJavascript\FieldFormatter
 *
 * @group media_entity_flourish
 */
class FlourishEmbedFormatterTest extends MediaJavascriptTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field_ui',
    'media_entity_flourish',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    \Drupal::configFactory()
      ->getEditable('media.settings')
      ->set('standalone_url', TRUE)
      ->save(TRUE);

    $this->container->get('router.builder')->rebuild();
  }

  /**
   * A test for the render of the 'flourish_embed' formatter.
   *
   * Without description.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testRenderWithoutDescription(): void {
    [$type, $id, $name] = [
      'visualization',
      '1087376',
      'Foo'
    ];
    $account = $this->drupalCreateUser(['view media']);
    $this->drupalLogin($account);
    $media_type = $this->createMediaType('flourish');

    $source = $media_type->getSource();
    $source_field = $source->getSourceFieldDefinition($media_type);

    $media_display = EntityViewDisplay::create([
      'targetEntityType' => 'media',
      'bundle' => $media_type->id(),
      'mode' => 'full',
      'status' => TRUE,
    ])->setComponent($source_field->getName(), [
      'type' => 'flourish_embed',
      'settings' => [],
    ]);
    $media_display->save();

    $entity = Media::create([
      'bundle' => $media_type->id(),
      'name' => $name,
      $source_field->getName() => [
        'type' => $type,
        'value' => $id,
      ],
    ]);
    $entity->save();

    $this->drupalGet($entity->toUrl());
    $assert = $this->assertSession();
    $element = $assert->elementExists('css', '.flourish-embed');
    $this->assertEquals("{$type}/{$id}", $element->getAttribute('data-src'));
    $this->assertEquals("https://flo.uri.sh/{$type}/{$id}/embed", $element->getAttribute('data-url'));
    $this->assertEquals($name, $element->getAttribute('aria-label'));
    $assert->waitForElementVisible('css', '.flourish-embed > iframe');
  }

  /**
   * A test for the render of the 'flourish_embed' formatter.
   *
   * With description.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testRenderWithDescription(): void {
    [$type, $id, $name, $description] = [
      'story',
      '321221',
      'Foo',
      'Foo bar bam.',
    ];
    $account = $this->drupalCreateUser(['view media']);
    $this->drupalLogin($account);
    $media_type = $this->createMediaType('flourish');

    $source = $media_type->getSource();
    $source_field = $source->getSourceFieldDefinition($media_type);

    $media_display = EntityViewDisplay::create([
      'targetEntityType' => 'media',
      'bundle' => $media_type->id(),
      'mode' => 'full',
      'status' => TRUE,
    ])->setComponent($source_field->getName(), [
      'type' => 'flourish_embed',
      'settings' => [],
    ]);
    $media_display->save();

    $entity = Media::create([
      'bundle' => $media_type->id(),
      'name' => $name,
      $source_field->getName() => [
        'type' => $type,
        'value' => $id,
        'description' => $description,
      ],
    ]);
    $entity->save();

    $this->drupalGet($entity->toUrl());
    $assert = $this->assertSession();
    $element = $assert->elementExists('css', '.flourish-embed');
    $this->assertEquals("{$type}/{$id}", $element->getAttribute('data-src'));
    $this->assertEquals("https://flo.uri.sh/{$type}/{$id}/embed", $element->getAttribute('data-url'));
    $this->assertEquals($description, $element->getAttribute('aria-label'));
    $assert->waitForElementVisible('css', '.flourish-embed > iframe');
  }

}
