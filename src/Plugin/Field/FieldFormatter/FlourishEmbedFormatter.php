<?php

declare(strict_types = 1);

namespace Drupal\media_entity_flourish\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Class Flourish_Embed_Formatter.
 *
 * @package Drupal\media_entity_lottie\Plugin\Field\FieldFormatter
 *
 * @FieldFormatter(
 *   id = "flourish_embed",
 *   label = @Translation("Flourish embed"),
 *   field_types = {
 *     "flourish",
 *   },
 * )
 */
class FlourishEmbedFormatter extends FormatterBase {

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    /** @var \Drupal\media\MediaInterface $media */
    $media = $items->getEntity();
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => $this->getPluginId(),
        '#id' => $item->value,
        '#type' => $item->type,
        '#title' => (!empty($item->description) ? $item->description : $media->label()),
      ];
    }
    $elements['#attached'] = [
      'library' => [
        'media_entity_flourish/embed',
      ],
    ];
    return $elements;
  }

}
